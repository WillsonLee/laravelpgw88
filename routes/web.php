<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/step1', function () {
    return view('step1');
});
*/

Route::view('/test',"test");
Route::view('/mbtest',"mbtest");
Route::post('/pgw',"dpgwctrl@seperate");
Route::get('/timeout',"dpgwctrl@timeout");
Route::post('/mbpgw',"mbpgwctrl@seperate");

Route::post('/step2',"dpgwctrl@captureusername");
Route::post('/captureans',"dpgwctrl@captureans");


//Route::view('/mbstep1',"mbstep1");
//Route::post('/mbstep2',"mbpgwctrl@fromsubmit");
//Route::post('/mbstep3',"mbpgwctrl@fromsubmit2");


