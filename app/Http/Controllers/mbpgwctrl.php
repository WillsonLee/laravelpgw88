<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Session;
use Config;

class mbpgwctrl extends Controller
{
    public $required = array('merchant', 'amount', 'refid', 'token', 'customer', 'currency', 'bankcode', 'clientip', 'post_url', 'failed_return_url', 'return_url');
    public $myStat=0;
    public $tx_http_post = "https://pgw.surepay88.com/fundtransfer/surereturn.php";

    public function seperate(Request $req){
    
            error_log('this is mobile : ' );
        error_log('count req : ' . count($req->all()) );
        if(count($req->all()) == 14 && $req->has('merchant') && $req->has('amount')){
            //count 14 means init send in de data
            error_log('yes is 14 : ' );
            return $this->start($req);

        }elseif(count($req->all()) == 5 && $req->has('username') && $req->has('password')){
            //count 5 means token,user,pass,x,y for step one , username and password
            error_log('yes is 5 : ' );
            return $this->captureusername($req);
            
        }elseif(count($req->all()) == 2 && $req->has('answer') ){
            //count 2 means token,answer for step 2 QnA
            error_log('yes is 2 : ' );
            return $this->captureans($req);

        }elseif(count($req->all()) == 4  &&$req->has('answer')){
            //count 4 means token,txttag,x,y for step 2 QnA
            return $this->captureans($req);
        }else{
            dd($req);
        }
    }

    public function start(Request $req){
        //////////////////////////////////////////////
        //if ((time() - Session::activity()) > (Config::get('session.lifetime') * 60))
        error_log('count req : ' . count($req->all()) );
        //dd($req);
        error_log('time : ' . time() );
        error_log('lifetime_min: ' . Config::get('session.lifetime'));
        session(['logintime' => time()]);
        session(['lifetime_min' => Config::get('session.lifetime')]);

        //error_log('$_SESSION-LAST_ACTIVITY: ' . session['LAST_ACTIVITY'] );
            // Session expired
            

        ////////////////////////////////////////////

        //test line
        $vars = $req->input("merchant") .','.$req->input("amount") .','.$req->input("refid") .','.$req->input("token") .','.$req->input("customer") .','.$req->input("currency").','.$req->input("bankcode").','.$req->input("clientip").','.$req->input("post_url").','.$req->input("failed_return_url").','.$req->input("return_url");
        
        $error = false;
        foreach($this->required as $field) {
            if (empty($req->input($field))) {
            error_log("empty is $field"); 
            $error = true;
            }
        }
            
        if ($error) {
            error_log("Mandatory fields NOT set.........",0);
            exit;
        } 

        // Assign post values into local variables
        $mymerchant = $req->input("merchant");
        //error_log("input amount is " .  $req->input("amount")); 
        $myamount = preg_replace("/[^0-9\.]/", "", $req->input("amount"));
        //error_log("output amount is " .  $myamount); 
        $myrefid = $req->input("refid");
        $mytoken = $req->input("token");
        $mycustomer = $req->input("customer");
        $mycurrency = $req->input("currency");
        if (!empty($req->input("language"))) {
            $mylanguage = $req->input("language");
        } else {
            $mylanguage = "MY";
        }
        /*
        "MAYBANK"
        "CIMB"
        "PBE"
        "HLB"
        "BSN"
        "DBS"
        "POSB"
        "UOB"
        "OCBC"
        "BCA"
        */
        $mybankcode = $req->input("bankcode");
        $myclientip = $req->input("clientip");
        $mypost_url = $req->input("post_url");
        $myfailed_return_url = $req->input("failed_return_url");
        $myreturn_url = $req->input("return_url");
            
        session(['smyfailed_return_url' => $myfailed_return_url]);
        session(['smyreturn_url' => $myreturn_url]);
        session(['smypost_url' => $mypost_url]);

        // Access DB to retreive the merchant data
        $Result = DB::table('pg_customer')
                ->select(DB::raw('id,username,email'))
                ->where('username', '=', $mymerchant)
                ->where('cust_type', '=', 'MERCHANT')
                ->get();

        if (count($Result) < 1) {
            error_log('surepay88 merchant id NOT FOUND '.$mymerchant,0);
            exit;
        } else {
            $mymerchantid = $Result[0]->id;
            error_log('surepay88 merchant id is '.$mymerchantid,0);
        }
            error_log('done result 1');
            $Result2 = DB::select('SELECT a.id, a.apikey, b.bank_id, a.MDR_in, a.MDR_out FROM pg_contracts a, pg_bankacounts b WHERE b.iban =:iban AND customer_id =:custid AND a.id = b.contracts_id', ['iban'=>$mybankcode, 'custid'=>$mymerchantid]);       
        
        if (count($Result2) > 0) {
                
            $myapikey = $Result2[0]->apikey;
            $myconstractid = $Result2[0]->id;
            $mybank_id = $Result2[0]->bank_id;
            $mymdr_in = $Result2[0]->MDR_in;
            $mymdr_out = $Result2[0]->MDR_out;
        }

        // compare MD5({merchant}{amount}{refid}{customer}{apikey}{currency}{clientip}) 
        $md5string = $mymerchant . $myamount . $myrefid . $mycustomer . $myapikey . $mycurrency . $myclientip;
        $md5str = md5($mymerchant . $myamount . $myrefid . $mycustomer . $myapikey . $mycurrency . $myclientip); 

        error_log('surepay88 token is '.$mytoken,0);
        error_log('surepay88 md5 str is '.$md5string,0);
        error_log('surepay88 md5 is '.$md5str,0);

        if ($mytoken != $md5str) {
            error_log('surepay88 token NOT VALID ');
            exit;
        }
        error_log("post_url is " . $req->input("post_url"));
        error_log("failed_return_url is " . $req->input("failed_return_url"));
        error_log("return_url is " . $req->input("return_url"));

        // Find the bank transfer info
        $Result3 = DB::select('SELECT a.iban FROM pg_bankacounts a, pg_banktransfers b where b.contract_id =:myconstractid and b.source_bank_id =:mybank_id and a.status = 0 and a.contracts_id = b.contract_id and b.dest_bank_id  = a.bank_id', ['myconstractid'=>$myconstractid, 'mybank_id'=>$mybank_id]);       
        if (count($Result3) > 0) {
            $mydestcode = $Result2[0]->iban;
        } else {
            $mydestcode = $mybankcode;
        }

        error_log("Get the source bank code");

        //// Get the source bank code
        $Result4 = DB::select('SELECT a.iban, a.id, a.bank_acc_no, b.code  FROM pg_bankacounts a, pg_banks b where a.iban =:mybankcode and a.bank_id = b.id and status = 0 and a.contracts_id =:myconstractid', ['mybankcode'=>$mybankcode, 'myconstractid'=>$myconstractid]);       
        if (count($Result4) > 0) {
            $mybankcode = $Result4[0]->code;
        }

        error_log("Find the BANK and accounts based on bankcode submitted");
        $sql = "SELECT a.iban, a.id, a.bank_acc_no, b.code  FROM pg_bankacounts a, pg_banks b where a.iban = '" . $mydestcode . "' and a.bank_id = b.id and status = 0 and a.contracts_id = " . $myconstractid;

        //$Result5 = DB::select('SELECT a.iban, a.id, a.bank_acc_no, b.code  FROM pg_bankacounts a, pg_banks b where a.iban =:mydestcode and a.bank_id = b.id and status = 0 and a.contracts_id =:myconstractid', ['mydestcode'=>$mydestcode, 'myconstractid'=>$myconstractid]);       
        $Result5 = DB::select($sql);
        if (count($Result5) > 0) {
            $mybank_acc_no = $Result5[0]->bank_acc_no;
            $mydestcode = $Result5[0]->code;
            $mybankid = $Result5[0]->id;
        }

        $md5str = $myrefid . $myamount . $mycustomer;
        $pgtoken = md5($md5str);
            
        error_log('$myrefid is '.$myrefid);
        error_log('$myamount is '.$myamount);
        error_log('$mycustomer is '.$mycustomer);
        error_log('$md5str is '.$md5str);
        error_log('$mybankcode is '.$mybankcode);

        //set all var to session
        session(['spgtoken' => $pgtoken]);
        session(['smyrefid' => $myrefid]);
        session(['smyamount' => $myamount]);
        session(['smycustomer' => $mycustomer]);
        session(['smybankcode' => $mybankcode]);
        session(['smybank_acc_no' => $mybank_acc_no]);
        session(['smydestcode' => $mydestcode]);
        $myVar = session('spgtoken');

            error_log(" md5(md5str) is " .  md5($md5str));
            error_log("myVar is " . $myVar);
        // Insert into pg_transactions with status is 0 
        $timestamp = date("Y-m-d H:i:s");

        $vSQL = "INSERT INTO pg_transactions (timestamp,lastupdate,telephone,email,status_msg,item_desc,trx_token, incoming_token, merchant, customer, type, contracts_id, bank_acc_id, item, status, amount, MDR_in, MDR_out, post_url,client_ip, created)";
        //$vSQL .= " values ('" . $pgtoken . "', '" . $mytoken . "','" . $mymerchant . "', '" . $mycustomer . "', 'd', $myconstractid, $mybankid, '" . $myrefid . "','0',$myamount, $mymdr_in, $mymdr_out, '" . $mypost_url . "','" . $myclientip . "', '" . $timestamp . "')";
        $vSQL .= " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        //temp close, tihs is for update info to db
        
        //try{
        //DB::Insert($vSQL,[now(),now(),'telephone','email_1','status_msg_1','item_desc_1',$pgtoken,$mytoken,$mymerchant,$mycustomer,'d',$myconstractid,$mybankid,$myrefid,'0',$myamount,$mymdr_in,$mymdr_out,$mypost_url,$myclientip,$timestamp]);
        //}catch(\Illuminate\Database\QueryException $qe){
        //    error_log("ERROR: Could not able to execute is " . $qe->getMessage());
        //    return view("step1")->with(["var"=>$qe->getMessage()]);
        //}
        
        //final output the page
        return view("mbstep1")->with(["logintime" => session('logintime'),"lifetime_min" => session('lifetime_min'),"bankcode" => session('smybankcode'),]);
        //return view("step1");
    }

    // request data from external API get username
    public function captureusername(Request $req){
                
        error_log('count captureusername req : ' . count($req->all()) );
        //dd($req);
        $username = $req->input("username");
        $myuserpass = $req->input("password");
        //put-back
        //$url = "https://tras.surepay88.com/v3/payinitiate";
	    //$url = "http://35.198.229.211/payinitiate";
	    $url = "http://127.0.0.1:8080/payinitiate";
        error_log("start : " );
        try {
            //put-back
            $client = new \GuzzleHttp\Client();
            
            $response = $client->POST($url,['json' => [
                    'ref_id' => session('spgtoken'),
                    'inv' => session('smyrefid'),
                    'amt' => session('smyamount'),
                    'player_id' => session('smycustomer'),
                    'username' => $username,
                    'bank_code' => session('smybankcode'),
                    'bankaccno' => session('smybank_acc_no'),
                    'dest_bank_code' => session('smydestcode'),
                    'client_ip' => '35.201.202.203'
            ]]);
            /*
            $myObj =(object)array();

            $myObj->ref_id = session('spgtoken');
            $myObj->status = 1;
            $myObj->message = "Password Required";
            $response = json_encode($myObj);
            
            error_log('myJSON : ' . $response);
            */
            //put-back
            $body = json_decode($response->getBody());
            //$body = json_decode($response);
            //put-back
            error_log('username response->getBody : ' . $response->getBody());
            //error_log('body->message : ' . $body->message);

           

            error_log("start2 : ");
                //error_log( 'I getStatusCode! ' . $response->getStatusCode());
                //error_log( 'I getReasonPhrase! ' . $response->getReasonPhrase());
            
            //put-back
            if($response->getReasonPhrase() == 'OK' && $response->getStatusCode() == '200'){
            //if ($body->status == 1) {
                //error_log( 'Status ' . $body->status .' , ' . $body->message);

                if ($body->status == 1) {
                    //after confirm username, now go check password
                    return $this->capturepassword($myuserpass);
                }else{
                    return $this->tx_failure($body->ref_id,$body->status,$body->message);
                }
            }

        } catch (RequestException $ex) {
            error_log("start3 : catch");
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            error_log("error : " .$jsonBody);
        }
    }
    
    public function capturepassword($myuserpass){
        
        //$url = "https://tras.surepay88.com/v3/paypassword";
        $url = "http://127.0.0.1:8080/paypassword";

        error_log("start pass : ");
        
        try {
            $client = new \GuzzleHttp\Client();
            
            error_log("session('spgtoken') : " . session('spgtoken'));
            error_log("myuserpass : " . $myuserpass);
                         
            $response = $client->POST($url,['json' => [
                    'ref_id' => session('spgtoken'),
                    'password' => $myuserpass
            ]]);
            
            error_log("response->getBody() : " .$response->getBody());
            $body = json_decode($response->getBody());
            if($response->getReasonPhrase() == 'OK' && $response->getStatusCode() == '200'){
                error_log( 'I get body! ' . $response->getBody());
                error_log($body->status . ", " . $body->question . ", " . $body->message);
                if($body->status == 1){
                    error_log('goto req4more');
		            return $this->req4more($body->ref_id,$body->status,$body->question,$body->message);
                }else{
                    error_log('goto status loading');
                    $innerhtml = 'Connecting to Bank...';
                    return view("status")->with(['htmlcode'=> $innerhtml , 'return_url'=> session('smyfailed_return_url')]);
                }
            }
        } catch (RequestException $ex) {
            error_log("error passAPI : catch");
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            //error_log("error : " .$jsonBody);
        }
    }
    
    public function tx_finalstate($myref,$mystatus,$mystatmsg) {
    
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $datenow = date("Y-m-d   h:i A.", time());
        $myStat=1;
        if ($mystatus == 1 ) {
	        error_log("success transfer");
        } else {
	        error_log("failed transfer");
        }

        if ($mystatus == 1) { //success
            $status = 'Accepted!';
            $htmlcode = '<h2>Transaction '. $status .'</h2><br><a href="'. session('myfailed_return_url') .'" class="btn btn-block btn-success">Returning to Merchant</a>';
            return view("mbstep3")->with(['return_url'=>session('smypost_url'),"amt" => session('smyamount'),"bankcode" => session('smybankcode'),"datenow" => $datenow,"tranid" => session('smyrefid'),]); 

            //setTimeout(function(){
            //        window.location = tx_return_url;
            //}, 5000);

        } else {

            $status = 'Rejected.';
            $htmlcode = '<h2>Transaction '. status .'</h2><br><small>Transaction Rejected. Wrong TAC or insufficient funds. Please initiate a new payment. Your account has not been charged.</small><br><br><a href="'. session('myfailed_return_url') .'" class="btn btn-block btn-danger">Returning to Merchant</a>';
            return view("status")->with(['return_url'=>session('smyreturn_url'), 'htmlcode'=> $htmlcode]); 
	        //setTimeout(function(){
		    //    window.location = tx_failed_return_url;
	        //}, 7000);

        }
    }

    public function tx_failure($myref,$mystatus,$mystatmsg){
        $myStat=1;
        if ($mystatus === 1 ) {
	        error_log("success transfer");
        } else {
	        error_log("failed transfer");
        }
        $request = $client->createRequest('POST', $this->tx_http_post);
        $request->setHeader('Content-type', 'application/x-www-form-urlencoded');
        $postBody = $request->getBody();

        // $postBody is an instance of GuzzleHttp\Post\PostBodyInterface
        $postBody->setField('transaction', session('smyrefid'));
        $postBody->setField('amount', session('smyamount'));
        $postBody->setField('status', $mystatus);
        $postBody->setField('status_message', $mystatmsg);
        $postBody->setField('bank_name', session('smybankcode'));
        $postBody->setField('signature', $myref);
        
        error_log(' postBody->getFields : ' . $postBody->getFields());
       
        //Call a function when the state changes.
        if($response->getReasonPhrase() == 'OK' && $response->getStatusCode() == '200'){
                error_log("receive " . $this->tx_http_post . " response");
            }
            
        // Send the POST request
        $response = $client->send($request);

        $status = 'Failed';
        $htmlcode = '<h2>Transaction '. $status.'  </h2><br><small>Transaction Failed. It seems like something has gone wrong. Please initiate a new payment or contact Merchant for support.</small><br><br><a href="'. session('myfailed_return_url') .'" class="btn btn-block btn-danger">Returning to Merchant</a>';
        return view("status")->with($htmlcode); 
    }

    public function req4more($ref_id,$mystatus,$myquestion,$mymessage){
        error_log('in other function');
        date_default_timezone_set('Asia/Kuala_Lumpur');
        
        $datenow = date("Y-m-d   h:i A.", time());
        if ($mystatus != 1) {
            error_log("mystatus != 1 " .$mystatus );
		    return $this->tx_failure($ref_id,$mystatus,$mymessage);
	    } else {
	        if ($myquestion == "none") {
                //finish the procress
                error_log("myquestion == none");
		        return $this->tx_finalstate($ref_id,$mystatus,$mymessage);
	        } else if($myquestion == "Enter the Tag") {
                //go to tag code
                error_log("in the Tag");
                return view('mbstep2')->with(["qna"=> "2","ques"=> $myquestion,"msg"=> $mymessage,"amt" => session('smyamount'),"bankcode" => session('smybankcode'),"datenow" => $datenow,"tranid" => session('smyrefid'),"logintime" => session('logintime'),"lifetime_min" => session('lifetime_min'),]);
            }else{
                //go to QnA
                return view('mbstep2')->with(["qna"=> "1","ques"=> $myquestion,"msg"=> $mymessage,"amt" => session('smyamount'),"bankcode" => session('smybankcode'),"datenow" => $datenow,"tranid" => session('smyrefid'),"logintime" => session('logintime'),"lifetime_min" => session('lifetime_min'),]);
            }
        }
    }

    /*
    public function captureans(Request $req) {
    
        error_log('count captureans req : ' . count($req->all()) );
        //dd($req);
        error_log('in captureans!444 : ');
        $myuserans = $req->input("myans");
	    //beep();
	    $myStat=1;
        $myObj =(object)array();
        // you need to continue ask customer with the question and message responded from
        //payAnswer. When customer key in the answer, you send the answer to /payAnswer again.

        $myObj->ref_id = session('spgtoken');
        $myObj->status = 1;
        if(session('question_change') == 'none'){
            $myObj->question = "none";
        }else{
            $myObj->question = "question";
        }
        $myObj->message = "Your first car brand?";

            session(['question_change' =>'none']);
        
         //the whole process end, you redirect customer back to merchant site
         //$myObj->ref_id = session('spgtoken');
         //$myObj->status = 1;
         //$myObj->question = "none";
         //$myObj->message = "success";
        
        $response = json_encode($myObj);
            
        error_log('myJSON : ' . $response);
        $body = json_decode($response);
        //putback
        //$url = "https://tras.surepay88.com/v3/payanswer";
        error_log("start captureans : ");

        try {
            //put-back
            //$client = new \GuzzleHttp\Client();
            // $response =  $client->POST($url,['json' => [
            //        'ref_id' => session('spgtoken'),
            //         'answer' => $myuserans
            //     ]
            //]);

            //put-back
            //error_log("response->getBody() : " .$response->getBody());
            //$body = json_decode($response->getBody());

            //error_log("start2 : " . $url);
            //put-back
            //if($response->getReasonPhrase() == 'OK' && $response->getStatusCode() == '200'){
            //delete
            if($body->status == 1){
                //error_log( 'I captureans! ' . $response->getBody());
                error_log($body->status . ", " . $body->question . ", " . $body->message);
                error_log('goto req4more from ans');
		        return $this->req4more($body->ref_id,$body->status,$body->question,$body->message);
            }else{
                error_log('goto status loading captureans ');
                $innerhtml = 'Processing payment...';
                return view("status")->with(['htmlcode'=> $innerhtml , 'return_url'=> session('smyfailed_return_url')]);
            } 

        } catch (RequestException $ex) {
            error_log("error ans API : catch");
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            //error_log("error : " .$jsonBody);
        }
    }
    */

    public function captureans(Request $req) {
    
        error_log('count captureans req : ' . count($req->all()) );
        //dd($req);
        error_log('in captureans!444 : ');
        $myuserans = $req->input("answer");
        //$url = "https://tras.surepay88.com/v3/payanswer";
        $url = "http://127.0.0.1:8080/payanswer";
        error_log("start captureans : ");

        try {
            $client = new \GuzzleHttp\Client();
            $response =  $client->POST($url,['json' => [
                    'ref_id' => session('spgtoken'),
                     'answer' => $myuserans
                 ]
            ]);

            error_log("response->getBody() : " .$response->getBody());
            $body = json_decode($response->getBody());

            error_log("start2 : " . $url);
            if($response->getReasonPhrase() == 'OK' && $response->getStatusCode() == '200'){
                error_log( 'I captureans! ' . $response->getBody());
                error_log($body->status . ", " . $body->question . ", " . $body->message);
                error_log('goto req4more from ans');
		        return $this->req4more($body->ref_id,$body->status,$body->question,$body->message);
            }else{
                error_log('goto status loading captureans ');
                $innerhtml = 'Processing payment...';
                return view("status")->with(['htmlcode'=> $innerhtml , 'return_url'=> session('smyfailed_return_url')]);
            } 

        } catch (RequestException $ex) {
            error_log("error ans API : catch");
            $response = $ex->getResponse();
            $jsonBody = (string) $response->getBody();
            error_log("jsonBody : " .$jsonBody);
        }
    }

    public function timeout() {
        $htmlcode = 'Session Timeout';
        return view("status")->with(['return_url'=>session('smyreturn_url'), 'htmlcode'=> $htmlcode]); 
    }



}
