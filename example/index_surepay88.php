<?php


// Required field names
$required = array('merchant', 'amount', 'refid', 'token', 'customer', 'currency', 'bankcode', 'clientip', 'post_url', 'failed_return_url', 'return_url');

// Loop over field names, make sure each one exists and is not empty
$error = false;
foreach($required as $field) {
  if (empty($_POST[$field])) {
    error_log("empty is $field"); 
    $error = true;
  }
}

if ($error) {
  error_log("Mandatory fields NOT set.........",0);
  exit;
} 

// Assign post values into local variables
$mymerchant = $_POST["merchant"];
error_log("input amount is " .  $_POST["amount"]); 
$myamount = preg_replace("/[^0-9\.]/", "", $_POST["amount"]);
//$myamount = $_POST["amount"];
error_log("output amount is " .  $myamount); 
$myrefid = $_POST["refid"];
$mytoken = $_POST["token"];
$mycustomer = $_POST["customer"];
$mycurrency = $_POST["currency"];
if (!empty($_POST["language"])) {
	$mylanguage = $_POST["language"];
} else {
	$mylanguage = "MY";
}
$mybankcode = $_POST["bankcode"];
$myclientip = $_POST["clientip"];
$mypost_url = $_POST["post_url"];
$myfailed_return_url = $_POST["failed_return_url"];
$myreturn_url = $_POST["return_url"];

// Access DB to retreive the merchant data
$mysqli = new mysqli("localhost", "pgadmin", "Qwer123$", "pgdb");

$sql = "SELECT id FROM pg_customer where username = '" . $mymerchant . "' and cust_type = 'Merchant'";
$result = mysqli_query($mysqli, $sql);
if (mysqli_num_rows($result) < 1) {
	error_log('surepay88 merchant id NOT FOUND '.$mymerchant,0);
	exit;
} else {
	while($row = mysqli_fetch_assoc($result)) {
	$mymerchantid = $row["id"];
	error_log('surepay88 merchant id is '.$mymerchantid,0);
	}
}

$sql = "SELECT a.id, a.apikey, b.bank_id, a.MDR_in, a.MDR_out FROM pg_contracts a, pg_bankacounts b where b.iban = '" . $mybankcode . "' and customer_id = " . $mymerchantid . " and a.id  = b.contracts_id";
$result = mysqli_query($mysqli, $sql);
if (mysqli_num_rows($result) > 0) {
	$row = mysqli_fetch_assoc($result);
	$myapikey = $row["apikey"];
	$myconstractid = $row["id"];
	$mybank_id = $row["bank_id"];
	$mymdr_in = $row["MDR_in"];
	$mymdr_out = $row["MDR_out"];
}

// compare MD5({merchant}{amount}{refid}{customer}{apikey}{currency}{clientip}) 
$md5string = $mymerchant . $myamount . $myrefid . $mycustomer . $myapikey . $mycurrency . $myclientip;
$md5str = md5($mymerchant . $myamount . $myrefid . $mycustomer . $myapikey . $mycurrency . $myclientip); 

error_log('surepay88 token is '.$mytoken,0);
error_log('surepay88 md5 str is '.$md5string,0);
error_log('surepay88 md5 is '.$md5str,0);

if ($mytoken != $md5str) {
	error_log('surepay88 token NOT VALID ');
	exit;
}
error_log("post_url is " . $_POST["post_url"]);
error_log("failed_return_url is " . $_POST["failed_return_url"]);
error_log("return_url is " . $_POST["return_url"]);

// Find the bank transfer info
$sql = "SELECT a.iban FROM pg_bankacounts a, pg_banktransfers b where b.contract_id = " . $myconstractid . " and b.source_bank_id = " . $mybank_id . " and a.status = 0 and a.contracts_id = b.contract_id and b.dest_bank_id  = a.bank_id";
$result = mysqli_query($mysqli, $sql);
if (mysqli_num_rows($result) > 0) {
	$row = mysqli_fetch_assoc($result);
	$mydestcode = $row["iban"];
} else {
	$mydestcode = $mybankcode;
}

// Get the source bank code
$sql = "SELECT a.iban, a.id, a.bank_acc_no, b.code  FROM pg_bankacounts a, pg_banks b where a.iban = '" . $mybankcode . "' and a.bank_id = b.id and status = 0 and a.contracts_id = " . $myconstractid;
error_log("select bankkcode sql is " . $sql);
$result = mysqli_query($mysqli, $sql);
if (mysqli_num_rows($result) > 0) {
	$row = mysqli_fetch_assoc($result);
	$mybankcode = $row["code"];
}


// Find the BANK and accounts based on bankcode submitted
$sql = "SELECT a.iban, a.id, a.bank_acc_no, b.code  FROM pg_bankacounts a, pg_banks b where a.iban = '" . $mydestcode . "' and a.bank_id = b.id and status = 0 and a.contracts_id = " . $myconstractid;
error_log("select bankkcode sql is " . $sql);
$result = mysqli_query($mysqli, $sql);
if (mysqli_num_rows($result) > 0) {
	$row = mysqli_fetch_assoc($result);
	$mybank_acc_no = $row["bank_acc_no"];
	$mydestcode = $row["code"];
	$mybankid = $row["id"];
}

$md5str = $myrefid . $myamount . $mycustomer;
$pgtoken = md5($md5str);


// Insert into pg_transactions with status is 0 
$timestamp = date("Y-m-d H:i:s");
$sql = "INSERT INTO pg_transactions (trx_token, incoming_token, merchant, customer, type, contracts_id, bank_acc_id, item, status, amount, MDR_in, MDR_out, post_url,client_ip, created) values ('" . $pgtoken . "', '" . $mytoken . "','" . $mymerchant . "', '" . $mycustomer . "', 'd', $myconstractid, $mybankid, '" . $myrefid . "','0',$myamount, $mymdr_in, $mymdr_out, '" . $mypost_url . "','" . $myclientip . "', '" . $timestamp . "')";
if(!mysqli_query($mysqli, $sql)){
        //print "ERROR: Could not able to execute $sql. " . mysqli_error($mysqli);
        error_log("ERROR: Could not able to execute $sql. " . mysqli_error($mysqli),0);
        header("Location: " . $myfailed_return_url);
}

echo '<html><head>';
echo '    <meta charset="utf-8">';
echo '    <meta http-equiv="X-UA-Compatible" content="IE=edge">';
echo '    <meta name="viewport" content="width=device-width, initial-scale=1.0">';
echo '    <title>Establishing a Secure Connection</title>';
echo '';
echo '<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
';
echo '<script src="/fundtransfer/newpayment.js"></script>';
session_start();
require_once("csrf.class.php");
$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);
echo '<script>';
echo 'var tx_token;';
echo 'var tx_inv;';
echo 'var tx_playerid;';
echo 'var tx_amt;';
echo 'var tx_bankcode;';
echo 'var tx_destcode;';
echo 'var tx_bank_acc_no;';
echo 'var tx_transid;';
echo 'var tx_tokenid;';
echo 'var tx_tokenvalue;';
echo 'var tx_lang;';
echo 'var tx_post_url;';
echo 'var tx_failed_return_url;';
echo 'var tx_return_url;';
echo '  $(document).ready(function(){';
echo '          tx_token = "' . $pgtoken . '";';
echo '          tx_inv = "' . $myrefid . '";';
echo '          tx_playerid = "' . $mycustomer . '";';
echo '          tx_amt = "' . $myamount . '";';
echo '          tx_bankcode = "' . $mybankcode . '";';
echo '          tx_destcode = "' . $mydestcode . '";';
echo '          tx_bank_acc_no = "' . $mybank_acc_no . '";';
echo '          tx_transid = "' . $myrefid . '";';
echo '          tx_lang = "' . $mylanguage . '";';
echo '          tx_tokenid = "' . $token_id . '";';
echo '          tx_tokenvalue = "' . $token_value . '";';
echo '          tx_post_url = "' . $mypost_url . '";';
echo '          tx_failed_return_url = "' . $myfailed_return_url . '";';
echo '          tx_return_url = "' . $myreturn_url . '";';
echo '          getPayload();';
echo '           $(document).ajaxStart(function () {';
echo '                  $("#state_desc").show();';
echo '              }).ajaxStop(function () {';
echo '              });';
echo '          });';
echo '';
echo '  </script>';
echo '';
echo '  <link href="/css/widget/my/base.css" rel="stylesheet">';
if ($mybankcode == "MAYBANK") {
echo '  <link rel="stylesheet" href="/css/widget/my/maybank.css">';
} else if ($mybankcode == "PBE") {
echo '  <link rel="stylesheet" href="/css/widget/my/pbe.css">';
} else if ($mybankcode == "HLB") {
echo '  <link rel="stylesheet" href="/css/widget/my/hlb.css">';
} else if ($mybankcode == "BSN") {
echo '  <link rel="stylesheet" href="/css/widget/my/bsn.css">';
} else if ($mybankcode == "RHB") {
echo '  <link rel="stylesheet" href="/css/widget/my/rhb.css">';
} else if ($mybankcode == "DBS") {
echo '  <link rel="stylesheet" href="/css/widget/sg/dbs.css">';
} else if ($mybankcode == "POSB") {
echo '  <link rel="stylesheet" href="/css/widget/sg/dbs.css">';
} else if ($mybankcode == "UOB") {
echo '  <link rel="stylesheet" href="/css/widget/sg/uob.css">';
} else if ($mybankcode == "BCA") {
echo '  <link rel="stylesheet" href="/css/widget/sg/uob.css">';
} else {
echo '  <link rel="stylesheet" href="/css/widget/my/cimb.css">';
}
echo '  <link rel="stylesheet" href="/css/widget/fontawesome/css/all.css">';
echo '                  ';
echo '</head>';
echo '';
echo '<body class="blank">';
echo ' <!-- Main content-->';
echo '<div class="container-fluid">';
echo '  <div class="row">';
echo '          <div class="panel">';
echo '                  <div class="panel-body">';
echo '                          <div class="modal-example">';
echo '                                  <style>';
echo '                                          /* Local style only for demo purpose */';
echo '                                          .modal-example .modal { position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: 1; display: block; }';
echo '                                  </style>';
echo '';
echo '                                  <div class="modal" tabindex="-1" role="dialog">';
echo '                                          <div class="modal-dialog modal-sm">';
echo '                                                  <div class="modal-content">';
echo '                                                          <div class="modal-header text-center">';
if ($mybankcode == "MAYBANK") {
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/my/maybank.jpg" height="75" class="img-circle">';
echo '                                                                  <h4>Maybank</h4><br>';
} else if ($mybankcode == "CIMB") {
//        header("Location: " . $myfailed_return_url);
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/my/cimb.jpg" height="75" class="img-circle">';
echo '                                                                  <h4>CIMB</h4><br>';
} else if ($mybankcode == "PBE") {
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/my/pbe.jpg" height="75" class="img-circle">';
echo '                                                                  <h4>Public Bank</h4><br>';
} else if ($mybankcode == "HLB") {
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/my/hlb.jpg" height="75" class="img-circle">';
echo '                                                                  <h4>Hong Leong</h4><br>';
} else if ($mybankcode == "BSN") {
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/my/bsn.jpg" height="75" class="img-circle">';
echo '                                                                  <h4>BSN</h4><br>';
} else if ($mybankcode == "DBS") {
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/sg/dbs.jpg" height="75" class="img-circle">';
echo '                                                                  <h4>DBS</h4><br>';
} else if ($mybankcode == "POSB") {
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/sg/posb.png" height="75" class="img-circle">';
echo '                                                                  <h4>POSB</h4><br>';
} else if ($mybankcode == "UOB") {
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/sg/uob.png" height="75" class="img-circle">';
echo '                                                                  <h4>UOB</h4><br>';
} else if ($mybankcode == "OCBC") {
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/sg/ocbc.jpg" height="75" class="img-circle">';
echo '                                                                  <h4>OCBC</h4><br>';
} else if ($mybankcode == "BCA") {
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/id/bca.jpg" height="75" class="img-circle">';
echo '                                                                  <h4>BCA</h4><br>';
} else {
echo '                                                                  <img src="https://pgw.surepay88.com/img/logo/my/rhb.jpg" height="75" class="img-circle">';
echo '                                                                  <h4>RHB</h4><br>';
}
echo '                                                                  <small class="stat-label">Amount to charge</small>';
//echo '                                                                        <h3 class="m-t-xs">MYR  10.00 </h3>';
echo '                                                                  <h3 class="m-t-xs">' . $mycurrency . ' ' .$myamount. '</h3>';
echo '                                                                  <br>';
echo '                                                                  <div class="progress m-t-xs full progress-small">';
echo '                                            <div id="state_progress" style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class=" progress-bar progress-bar-success">';
echo '                                            </div>';
echo '                                    </div>';
echo '';
echo '';
echo '                                                          </div>';
echo '                                                          <div class="modal-body text-center">';
echo '                                                                  <div class="row">';
echo '                                                                          <div id="state_desc"><i class="fa fa-cog fa-spin"></i><small>  Establishing a Secure Connection</small></div>';
echo '                                                                      <div id="result"></div>';
echo '                                                                      <form autocomplete="off" class="form-signin" id="container">';
echo '                                                                                  <input autocomplete="false" name="hidden" type="text" style="display:none;">';
echo '                                                                          <input type="hidden" name="_token" value="WfjAy3VMprnwHAeSAUx5jVJ3bsY0YMIs6xYX81Um">';
echo '                                                                      </form>';
echo '                                                                      <div id="keymessage"></div>';
echo '                                                                  </div>';
echo '';
echo '                                                          </div>';
echo '                                                          <div class="modal-footer text-left">';
echo '                                                                  <small> Please <strong>DO NOT REFRESH</strong> your web browser</small>';
echo '                                                          </div>';
echo '                                                  </div>';
echo '                                          </div>';
echo '                                  </div>';
echo '                          </div>';
echo '                  </div>';
echo '          </div>';
echo '  </div>';
echo '</div>';
echo '';
echo '';
echo '';
echo '';
echo '</body></html>';


