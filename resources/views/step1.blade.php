@extends('dpgw')

@section('title','step 1')

@section('script')
<script type="text/javascript">

    var timeout_time = {{$logintime}} + ({{$lifetime_min}} * 60);
    var now = parseInt(Date.now()/1000);
    var sec = timeout_time - now;

    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;

            if (--timer < 0) {
                timer = duration;
                console.log('timeout');
                window.location.href = "/timeout";
            }else{
                console.log('aftet -- : ' + timer);
            }
        }, 1000);
    }

    function countDown() {
        //var timer = document.getElementById("timer");
        if (sec > 0) {
            sec--;
            console.log(timeout_time);
            console.log(now);
            console.log(sec);
            console.log(parseInt(Date.now() / 1000));
            setTimeout("countDown()", 1000);
        } else {
        }
    }

    window.onload = function () {
        //var Minutes = 60 * {{$lifetime_min}};
            //console.log(Minutes);
            display = document.querySelector('#time');
            display2 = document.querySelector('#time2');
        startTimer(sec, display);
        startTimer(sec, display2);
    };

</script>
@endsection

@section('container')



<div class="container">
    <div class="step_holder">
        <div class="d-none d-lg-block">
            {{HTML::image('images/image/step1.jpg')}} 
        </div>
        <div class="d-sm-block d-md-block d-lg-none">
            {{HTML::image('images/mobile/step-1.jpg')}} 
        </div>
    </div>

    
    <div class="d-none d-lg-block">
    <table align="center">

          <tr>
            <td><img src="images/image/{{$bankcode}}.jpg" /></td>
            <td width="40"></td>
            
            <form action="pgw" method="POST">
                @csrf
                <td>
                    <div style="background-image:url(images/desktop/username.jpg); height:43px; width:305px;">
                        <table width="100%">
                            <tr>
                            <td valign="middle" width="35" height="43"></td>
                            <td valign="top"><input name="username" type="text" placeholder="ID Pengguna" value="Merchant 004" class="login_txt" /></td>
                            </tr>
                        </table>
                    </div>
        
                    <div style="background-image:url(images/desktop/password.jpg); height:43px; width:305px; margin:14px 0;">
                        <table width="100%">
                            <tr>
                            <td valign="middle" width="35" height="43"></td>
                            <td valign="top"><input name="password" type="password" placeholder="Kata Laluan" value="test1234" class="login_txt" /></td>
                            </tr>
                        </table>
                    </div>
        
                    <div><input type="image" src="images/desktop/btn-teruskkan.jpg" border="0" alt="Submit" /></div>

                </td>
            </form>

            </tr>`
            <tr>
            <td height="20">  </td>
            </tr>
            <tr>
            <td colspan="3" style="font-size:14px; color:#8b8b8b;">    *Secure Online Banking: Please Use your <span style="color:#3c3c3c; font-weight:bold;">Internet Banking</span> account and password to log in.<br />
            Anda akan dilog keluar dalam <span id="time" style="color:#6b1b1b; font-weight:bold;"></span> minit.</td>
          </tr>
    </table>
    </div>

    <div class="d-sm-block d-md-block d-lg-none">
    <table align="center">
      <tr>
        
            <tr align="center">
                <td><div style="padding-bottom:25px;"><img src="images/image/{{$bankcode}}.jpg" width=" 50%" /></div></td>
            </tr>
        <tr align="center">
            <form action="pgw" method="POST">
            @csrf
            <td>
                <div style="background-image:url(images/mobile/username.jpg); height:43px; width:305px;">
                <table width="100%">
                  <tr>
                    <td valign="middle" width="35" height="43"></td>
                    <td valign="top"><input name="username" type="text" placeholder="ID Pengguna" value="Merchant 004" class="login_txt" /></td>
                  </tr>
                </table>
            </div>
        
            <div style="background-image:url(images/mobile/password.jpg); height:43px; width:305px; margin:14px 0;">
                <table width="100%">
                  <tr>
                    <td valign="middle" width="35" height="43"></td>
                    <td valign="top"><input name="password" type="password" placeholder="Kata Laluan" value="test1234" class="login_txt" /></td>
                  </tr>
                </table>
            </div>
        
            <div><input type="image" src="images/mobile/btn-teruskkan.jpg" border="0" alt="Submit" /></div>
            </td>
            </form>
            </tr>`
            <tr>
            <td height="20">  </td>
            </tr>
            <tr>
            <td colspan="3" style="font-size:14px; color:#8b8b8b;">    *Secure Online Banking: Please Use your <span style="color:#3c3c3c; font-weight:bold;">Internet Banking</span> account and password to log in.<br />
            Anda akan dilog keluar dalam <span id="time2" style="color:#6b1b1b; font-weight:bold;"></span> minit.</td>
          </tr>
    </table>
    </div>
</div>
@endsection

