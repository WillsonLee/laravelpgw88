<html>
    <head>
    
        <link rel="stylesheet" href="{{asset('css/4.3.1.bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('css/pgw.css')}}">
        <title>@yield('title')</title>
    </head>

    <body">

        {{ csrf_field()}}
        @yield('container')
        <table align="center" class="footer">
            <tr>
            <td colspan="2"><div style="color:#8b8b8b; padding:20px 0 14px;">Peringatan:</div></td>
            </tr>
            <tr>
            <td valign="top">•</td>
            <td>Anda telah mengaktifkan keupayaan pemprosesan pembayaran dalam talian pada akuan bank anda.</td>
            </tr>
            <tr>
            <td valign="top">•</td>
            <td>Sila jangan muat semula (refresh) halaman semasa proses.</td>
            </tr>
            <tr>
            <td valign="top">•</td>
            <td>Bank akuan anda mempunyai baki yang mencukupi.</td>
            </tr>
            <tr>
            <td valign="top">•</td>
            <td>Sila ambil perhatian terhadap Nombor Rujukan Bank dalam mana-mana kes jabatan Perkhidmatan<br />Pelanggan kami mungkin perlu untuk pengesahan lanjut.</td>
            </tr>
        </table>
        @yield('script')
    </body>
</html>
