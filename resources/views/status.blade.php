@extends('dpgw')

@section('title','Loading')

@section('script')
 <link rel="stylesheet" href="{{asset('css/pgw.css')}}">
@endsection
@section('container')
<div class="container"   align="middle">
   <i class="fa fa-cog fa-spin"></i><h2>{!!$htmlcode!!}</h2>
</div>
@endsection

<script type="text/javascript">
var sec = 180;
 
function countDown(){
    var timer = document.getElementById("timer");
    if(sec > 0){
        sec--;
        //console.log(sec);
        setTimeout("countDown()", 1000);
    }else{
        //window.location.href = '{{$return_url}}';
        window.location = '{{$return_url}}';
    }
}

window.onload = countDown;
</script>


