@extends('dpgw')

@section('title','step 2')

@section('script')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script type="text/javascript">

    function run2(){
    
        var myquestion = '{{$ques}}';
        var mymessage = '{!!$msg!!}';
            form = '<form action="pgw" method="POST" align="center"> ';
	        form += '@csrf';
            form += '<table> ';
	        form += '<tr>';
        /*
        <form action="pgw" method="POST">
                     @csrf
                  <tr>
                    <td id="answer"><input name="answer" type="text" class="tag_txt" /></td>
                    <td width="15"></td>
                    <td><input type="image" src="images/desktop/btn-mengesahkan.jpg" border="0" alt="Submit" /></td>
                  </tr>
                  </form>
        */
        if({{$qna}} == 1){
        
	        if (myquestion.includes('Please select which account to use.') && mymessage.includes('option value')) {
		        myquestion = 'Please select which account to use.';
		        question = '<label class="control-label"><strong>' + myquestion + '</strong></label> ';
		        form += '<td><select name="answer">' + mymessage + '</select></td> ';
	            form += '</br>';
	            $('#question').show();
	            $('#question').html(question);
	        } else {
            console.log("question");
		        form += '<td id="answer"><input name="answer" type="text" class="tag_txt" /></td> ';
	            form += '</br>';
	            $('#question').html(myquestion);
	        }
	        $('#tagonly').hide();
        }else{
        
	        form += '<td><input name="answer" type="text" class="tag_txt" /></td>';
        }

	    //form += '<td width="15"></td>';
	    form += '<td><input type="image" src="images/desktop/btn-mengesahkan.jpg" border="0" alt="Submit" /></td>';
	    form += '</tr>';
	    form += '</table>';
	    form += '</form>';



	    $('#form').show();
	    $('#form').html(form);
    }

    var timeout_time = {{$logintime}} + ({{$lifetime_min}} * 60);
    var now = parseInt(Date.now()/1000);
    var sec = timeout_time - now;

    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;

            if (--timer < 0) {
                timer = duration;
                console.log('timeout');
                window.location.href = "/timeout";
            }else{
                console.log('aftet -- : ' + timer);
            }
        }, 1000);
    }

    window.onload = function () {
        //var Minutes = 60 * {{$lifetime_min}};
            //console.log(Minutes);
            display = document.querySelector('#time');
            run2();
        startTimer(sec, display);
    };


</script>
@endsection
@section('container')
<div class="container">

    <div class="step_holder">
    <div class="d-none d-lg-block">
        <img src="images/desktop/step-2.jpg" />
    </div>
    <div class="d-sm-block d-md-block d-lg-none">
        <img src="images/mobile/step-2.jpg" width="100%" />
    </div>
    </div>
	<table width="80%" align="center">
      <tr>
        <td bgcolor="#f5f5f5" style="padding:40px 0;">
        
        <table width="70%" align="center">
          <tr>
            <td style="color:#191919; font-weight:bold; font-size:16px;">Ringkasan Urus Niaga</td>
          </tr>
          <tr>
            <td width="10"></td>
          </tr>
          <tr>
            <td><div style="width:100%; height:1px; background-color:#cbcbcb;"></div></td>
          </tr>
        </table>

        <table align="center" style="color:#363636; font-size:14px; margin-top:20px;">
          <tr>
            <td align="right">Bank</td>
            <td>:</td>
            <td width="40"></td>
            <td><img src="images/image/{{$bankcode}}.jpg" width="180" /></td>
          </tr>
          <tr>
            <td height="5"></td>
          </tr>
          <tr>
            <td align="right">ID Transaksi</td>
            <td>:</td>
            <td></td>
            <td><strong>{{$tranid}}</strong></td>
          </tr>
          <tr>
            <td height="2"></td>
          </tr>
          <tr>
            <td align="right">Jumlah</td>
            <td>:</td>
            <td></td>
            <td><strong>MYR {{$amt}}</strong></td>
          </tr>
          <tr>
            <td height="2"></td>
          </tr>
          <tr>
            <td align="right">Masa</td>
            <td>:</td>
            <td></td>
            <td><strong>{{$datenow}}</strong></td>
          </tr>
        </table>
        
        </td>
      </tr>
      <tr>
        <td>
        <div id="QATag" >
            <table  style="font-size:14px; color:#8b8b8b;" align="center">
              <tr>
                <td height="10"></td>
              </tr>
              <tr>
                <td style="color:#191919;"><strong id="question">Sila Masukkan TAC anda</strong></td>
              </tr>
              <tr>
                <td id="tagonly">{!!$msg!!}</td>
              </tr>
              <tr>
                <td height="10"></td>
              </tr>
              <td>
          	    <table>
                    <div id="form">
                     
                    </div>
                </table>
              </td>
              <tr>
                <td height="10"></td>
              </tr>
              <tr>
                <td>Anda akan dilog keluar dalam <span id="time" style="color:#6b1b1b; font-weight:bold;"></span> minit.</td>
              </tr>
            </table>
        </div
        
        <div id="keymessage"></div>
        </td>
      </tr>
    </table>
</div>
@endsection


