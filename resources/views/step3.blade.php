@extends('dpgw')

@section('title','step 3')

@section('script')

<script type="text/javascript">
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;

            if (--timer < 0) {
                timer = duration;
                window.location.href = '{{$return_url}}';
            }
        }, 1000);
    }

    window.onload = function () {
        var oneMinutes = 60 * 1,
            display = document.querySelector('#time');
        startTimer(oneMinutes, display);
    };
</script>
@endsection
@section('container')
<div class="container">
    
    <div class="step_holder">
        <div class="d-none d-lg-block">
            <img src="images/desktop/step-3.jpg" />
        </div>
        <div class="d-sm-block d-md-block d-lg-none">
            <img src="images/mobile/step-3.jpg" width="100%" />
        </div>
    </div>

	<table width="80%" align="center">
      <tr>
        <td bgcolor="#f5f5f5" style="padding:40px 0;">
        
        <table width="70%" align="center">
          <tr>
            <td style="color:#191919; font-weight:bold; font-size:16px;">Ringkasan Urus Niaga</td>
          </tr>
          <tr>
            <td width="10"></td>
          </tr>
          <tr>
            <td><div style="width:100%; height:1px; background-color:#cbcbcb;"></div></td>
          </tr>
        </table>

        <table align="center" style="color:#363636; font-size:14px; margin-top:20px;"">
          <tr>
            <td align="right">Bank</td>
            <td>:</td>
            <td width="40"></td>
            <td><img src="images/image/{{$bankcode}}.jpg" width="180" /></td>
          </tr>
          <tr>
            <td height="5"></td>
          </tr>
          <tr>
            <td align="right">ID Transaksi</td>
            <td>:</td>
            <td></td>
            <td><strong>{{$tranid}}</strong></td>
          </tr>
          <tr>
            <td height="2"></td>
          </tr>
          <tr>
            <td align="right">Jumlah</td>
            <td>:</td>
            <td></td>
            <td><strong>MYR {{$amt}}</strong></td>
          </tr>
          <tr>
            <td height="2"></td>
          </tr>
          <tr>
            <td align="right">Masa</td>
            <td>:</td>
            <td></td>
            <td><strong>{{$datenow}}</strong></td>
          </tr>
        </table>
        
        </td>
      </tr>
      
      <tr>
      	<td height="30"></td>
      </tr>
      
      <tr>
        <td> 
        
        <table align="center">
          <tr>
            <td valign="top"><img src="images/desktop/done-mark.jpg" /></td>
            <td width="20"></td>
            <td valign="top">
            <div style="color:#191919; font-size:14px; font-weight:bold; padding-bottom:4px;">Tranksaksi anda telah berjaya.</div>
            
            <div style="color:#8b8b8b; font-size:14px; padding-bottom:12px;">Anda akan dilog keluar dalam masa<span id="time" style="color:#6b1b1b; font-weight:bold;">01:00</span> minit.</div>
            
            <a href="{!!$return_url!!}" style="color:#387cbc; font-size:14px;">(Redirect sekarang)</a>
            </td>
          </tr>
        </table>

        
        </td>
      </tr>
    </table>

    
    
</div>
@endsection


